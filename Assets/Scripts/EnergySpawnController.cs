﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class EnergySpawnController : MonoBehaviour 
{
	[SerializeField]
	private float speed;
	
	[SerializeField]
	private float spawnTime;

	[SerializeField]
	private float spawnTimeDifference;

	private LevelSpawner levelSpawner;

	private List<Transform> energyItems;
	private List<Transform> toRemove;

	void Start () 
	{
		energyItems = new List<Transform>();
		toRemove = new List<Transform>();
		levelSpawner = GetComponent<LevelSpawner>();
	}

	void Update()
	{
		if(GameManager.Playing)
		{
			foreach(var energy in energyItems)
			{
				if(energy.position.y <= -5.56)
					toRemove.Add(energy);
				else
					energy.Translate(new Vector2(0, -speed * Time.deltaTime));
			}

			foreach(var energy in toRemove)
				energyItems.Remove(energy);

			toRemove.Clear();
		}
	}

	public void StartSpawning()
	{
		StartCoroutine(Spawn());
	}

	IEnumerator Spawn()
	{
		yield return new WaitForSeconds(Random.Range(spawnTime - spawnTimeDifference / 3, spawnTime + spawnTimeDifference));
		var energy = levelSpawner.SpawnEnergy();
		energy.transform.position = new Vector2(Random.Range(transform.position.x - 3, transform.position.x + 3), transform.position.y);
		energyItems.Add(energy.transform);
		StartCoroutine(Spawn());
	}
}
