﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathBlockController : MonoBehaviour 
{
	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.collider.tag == "Player")
		{
			GetComponent<AudioSource>().Play();
			var direction = (new Vector2(collision.collider.transform.position.x, collision.collider.transform.position.y) - collision.contacts[0].point).normalized;
			collision.collider.GetComponent<PlayerController>().GoInDirection(direction);
		}
	}
}
