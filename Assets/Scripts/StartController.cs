﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class StartController : MonoBehaviour 
{
	void Update()
	{
		if(Input.anyKeyDown)
			SceneManager.LoadScene("PlayScene");
	}
}
