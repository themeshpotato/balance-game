﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSpawner : MonoBehaviour 
{
	[SerializeField]
	private PlayerConnectionController playerConnectionController;
	
	[SerializeField]
	private GameObject blockPrefab;

	[SerializeField]
	private GameObject deathBlockPrefab;

	[SerializeField]
	private GameObject energyPrefab;

	private int blockIndex;
	private GameObject[] blockPool;

	private int deathBlockIndex;
	private GameObject[] deathBlockPool;

	private int energyIndex;
	private GameObject[] energyPool;

	void Start () 
	{
		blockPool = new GameObject[10];
		for(int i = 0; i < 10; i++)
		{
			blockPool[i] = Instantiate(blockPrefab) as GameObject;
			blockPool[i].SetActive(false);
		}

		deathBlockPool = new GameObject[10];
		for(int i = 0; i < 10; i++)
		{
			deathBlockPool[i] = Instantiate(deathBlockPrefab) as GameObject;
			deathBlockPool[i].SetActive(false);
		}

		energyPool = new GameObject[10];
		for(int i = 0; i < 10; i++)
		{
			energyPool[i] = Instantiate(energyPrefab) as GameObject;
			energyPool[i].GetComponent<EnergyController>().playerConnectionController = playerConnectionController;
			energyPool[i].SetActive(false);
		}

		GetComponent<BlockController>().StartSpawning();
		GetComponent<EnergySpawnController>().StartSpawning();
	}

	public GameObject SpawnBlock()
	{
		var nextBlock = blockPool[blockIndex++];
		if(blockIndex == blockPool.Length)
			blockIndex = 0;
		nextBlock.SetActive(true);
		return nextBlock;
	}

	public GameObject SpawnDeathBlock()
	{
		var nextBlock = deathBlockPool[deathBlockIndex++];
		if(deathBlockIndex == deathBlockPool.Length)
			deathBlockIndex = 0;
		nextBlock.SetActive(true);
		return nextBlock;
	}

	public GameObject SpawnEnergy()
	{
		var nextEnergy = energyPool[energyIndex++];
		if(energyIndex == energyPool.Length)
			energyIndex = 0;
		nextEnergy.SetActive(true);
		return nextEnergy;
	}
}
