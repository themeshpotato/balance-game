﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour 
{
	[SerializeField]
	private float speed;
	
	[SerializeField]
	private float spawnTime;

	[SerializeField]
	private float spawnTimeDifference;

	private LevelSpawner levelSpawner;

	private List<Transform> blocks;
	private List<Transform> toRemove;

	void Start () 
	{
		blocks = new List<Transform>();
		toRemove = new List<Transform>();
		levelSpawner = GetComponent<LevelSpawner>();
	}

	void Update()
	{
		if(GameManager.Playing)
		{
			foreach(var block in blocks)
			{
				if(block.position.y <= -7.56)
					toRemove.Add(block);
				else
					block.Translate(new Vector2(0, -speed * Time.deltaTime));
			}

			foreach(var block in toRemove)
				blocks.Remove(block);

			toRemove.Clear();
		}
	}

	public void StartSpawning()
	{
		StartCoroutine(Spawn());
	}

	IEnumerator Spawn()
	{
		yield return new WaitForSeconds(Random.Range(spawnTime - spawnTimeDifference / 3, spawnTime + spawnTimeDifference));
		var block = Random.Range(0, 100) < 40 ? levelSpawner.SpawnDeathBlock() : levelSpawner.SpawnBlock();
		
		var randomNum = Random.Range(0, 60);
		Vector3 position;

		if(randomNum <= 20)
			position = new Vector3(transform.position.x - 1.92f, transform.position.y, 10);
		else if(randomNum >= 21 && randomNum <= 40)
			position = new Vector3(transform.position.x, transform.position.y, 10);
		else
			position = new Vector3(transform.position.x + 1.95f, transform.position.y, 10);

		block.transform.position = position;

		blocks.Add(block.transform);
		StartCoroutine(Spawn());
	}
}
